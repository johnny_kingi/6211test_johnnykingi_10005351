﻿/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------Question 2 ------------------------------------------------------*/
/*---------------------------------Learning outcomes 1 & 2 - Data Types and Recurssion------------------------------------*/
/*----------------------------------------------------Marks - Out of 10----------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
/*------------------------------------------------------------------------------------------------------------------------*/
/*-----------Question 2 - Implement a recursive method to find a factorial using hardcoded and user input numbers---------*/
/*------------------------------------------------------Marks - Out of 10-------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
    class Program
    {
        static void Main(string[] args)
        /*Hardcoded and user input passed to the factorial recursive method(Marks out of 5)*/
        {
            Console.WriteLine("\n\tPlease enter a number to find the factorial of it:");
            int input = int.Parse(Console.ReadLine());

            int answer = FindFactorial(input);

            Console.WriteLine("\n\tFactorial of {0} is:\t{1}",input,answer);
            Console.ReadKey();

        }

        public static int FindFactorial(int factorial)
        /*Recursive method (Marks out of 5)*/
        {
            if (factorial == 1)
                return 1;
             
            return factorial * FindFactorial(factorial - 1);
        }
    }

/*------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------Question 2 END-------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
}
